package com.practice.corejava;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapDemo {

	public static void main(String[] args) {
		Map<Integer,Employee> hashMap = new HashMap<Integer, Employee>(10,0.75F);
		Employee emp1 = new Employee(101, "Sujan");
		Employee emp2 = new Employee(102, "Duong");
		Employee emp3 = new Employee(103, "Almaz");
		Employee emp4 = new Employee(104, "Amandeep");
		
		hashMap.put(emp1.getEmpId(), emp1);
		hashMap.put(emp2.getEmpId(), emp2);
		hashMap.put(emp3.getEmpId(), emp3);
		hashMap.put(emp4.getEmpId(), emp4);
		
		System.out.println("Displaying first approach:");
		Set<Map.Entry<Integer, Employee>> entries = hashMap.entrySet();
		for(Map.Entry<Integer, Employee> emp: entries) {
			System.out.println(emp.getKey());
			Employee employee = emp.getValue();
			employee.display();
		}
		
		System.out.println("\nDisplaying second approach:");
		Iterator<Integer> iterator = hashMap.keySet().iterator();
		while(iterator.hasNext()) {
			Employee emp = hashMap.get(iterator.next());
			emp.display();
		}

	}

}
//